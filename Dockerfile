FROM alpine:3.15.0
RUN apk add --no-cache openssh-client
ENTRYPOINT ["/usr/bin/ssh"]
